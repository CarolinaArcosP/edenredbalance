//
//  LoginViewController.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 7/29/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var sectionOneTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var sectionOneTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "Bienvenido a W-Viáticos"
            titleLabel.textColor = .darkGray
        }
    }
    @IBOutlet weak var subtitleLabel: UILabel!  {
        didSet {
            subtitleLabel.text = "Ingresa los 16 dígitos de tu tarjeta para continuar"
            subtitleLabel.textColor = .darkGray
        }
    }
    @IBOutlet weak var cardNumberTitle: UILabel!  {
        didSet  {
            cardNumberTitle.text = "Número de tarjeta"
            cardNumberTitle.textColor = UIColor.darkGray
        }
    }
    @IBOutlet weak var cardImageView: UIImageView!  {
        didSet {
            cardImageView.image = UIImage.card
        }
    }
    @IBOutlet weak var loginButton: CustomButton! {
        didSet {
            loginButton.setTitle("CONTINUAR", for: .normal)
            loginButton.setTitleColor(UIColor.white, for: .normal)
            loginButton.isEnabled = false
            loginButton.alpha = 0.5
        }
    }
    @IBOutlet weak var inputTextField: UITextField! {
        didSet {
            inputTextField.textColor = UIColor.lightBlack
            inputTextField.keyboardType = .numberPad
        }
    }
    @IBOutlet weak var buttonDistanceConstraint: NSLayoutConstraint!
    @IBOutlet weak var sectionOne: UIStackView!
    @IBOutlet weak var sectionTwo: UIStackView!
    @IBOutlet weak var sectionThree: UIStackView!
    @IBOutlet weak var sectionFour: UIStackView!
    
    // MARK: - Initializers

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTouchOutside()
        inputTextField.delegate = self
        inputTextField.addTarget(self,
                                 action: #selector(textfieldDidChange(_:)),
                                 for: UIControl.Event.editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotifications(keyboardShow: #selector(keyboardWillShow(notification:)),
                                      keyboardHide: #selector(keyboardWillHide(notification:)))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        unregisterNotifications()
        scrollView.contentInset.bottom = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
        
        sectionOneTrailingConstraint.constant = -(cardImageView.frame.width * 0.25)
        sectionOneTopConstraint.constant = -(cardImageView.frame.height * 0.44)
        loginButton.applyGradient(colours: [UIColor.lightEdentedBlue, UIColor.edenredBlue])
        
        let excess =  contentView.frame.height - scrollView.frame.height
        if excess < 0 {
            buttonDistanceConstraint.constant = buttonDistanceConstraint.constant - excess
        }
    }
    
    // MARK: - Actions

    @IBAction func login()  {
        guard let cardNumber = inputTextField.text?.replacingOccurrences(of: " ", with: ""),
            cardNumber.isNumeric(), cardNumber.count == cardNumberLenght else { return }
        
        saveCardNumber(number: cardNumber)
        let balanceViewController = EdenredBalanceViewController(nibName: "BalanceView", bundle: nil)
        balanceViewController.cardNumber = cardNumber
        present(balanceViewController, animated: true, completion: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        scrollView.contentInset.bottom = keyboardFrame.size.height + 10
    }
    
    @objc func keyboardWillHide(notification:  NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    @objc func textfieldDidChange(_ textField: UITextField) {
        let clearString = textField.text?.replacingOccurrences(of: " ", with: "")
        if let text = clearString,
            text.isNumeric(), text.count <= cardNumberLenght {
            
            textField.text = text.separate(every: 4, with: " ")
            
            switch text.count {
            case 0...4:
                let sectionOneString = text
                sectionOne.removeStackViewSubviews()
                sectionTwo.removeStackViewSubviews()
                sectionThree.removeStackViewSubviews()
                sectionFour.removeStackViewSubviews()
                sectionOne.setNumbersInStackview(text: String(sectionOneString))
            case 5...8:
                let endSectionOneIndex = text.index(text.startIndex, offsetBy: 4)
                let sectionTwoString = text[endSectionOneIndex...]
                sectionTwo.removeStackViewSubviews()
                sectionThree.removeStackViewSubviews()
                sectionFour.removeStackViewSubviews()
                sectionTwo.setNumbersInStackview(text: String(sectionTwoString))
            case 9...12:
                let endSectionTwoIndex = text.index(text.startIndex, offsetBy: 8)
                let sectionThreeString = text[endSectionTwoIndex...]
                sectionThree.removeStackViewSubviews()
                sectionFour.removeStackViewSubviews()
                sectionThree.setNumbersInStackview(text: String(sectionThreeString))
            case 13...16:
                let endSectionThreeIndex = text.index(text.startIndex, offsetBy: 12)
                let sectionFourString = text[endSectionThreeIndex...]
                sectionFour.removeStackViewSubviews()
                sectionFour.setNumbersInStackview(text: String(sectionFourString))
            default: return
            }
        }
        
        loginButton.isEnabled = clearString?.count == cardNumberLenght
        loginButton.alpha = clearString?.count == cardNumberLenght ? 1 : 0.5
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let clearString = string.replacingOccurrences(of: " ", with: "")
        let newLength = textField.text!.count + clearString.count - range.length
        
        guard string.isNumeric(), newLength <= cardNumberLenght + 3 else { return false }
        
        return true
    }
}
