//
//  DateExtension.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/10/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import Foundation

extension Date {
    func toStringWithFormat(_ format: String, locale: Locale = Locale(identifier: "en_US_POSIX")) -> String? {
        let formatter = DateFormatter()
        formatter.locale = locale
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
