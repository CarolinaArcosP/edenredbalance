//
//  BalanceViewModel.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/8/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import Foundation

class BalanceViewModel {
    
    let cardNumber: String
    let repository: BalanceRepository
    
    init(cardNumber: String, repository: BalanceRepository) {
        self.cardNumber = cardNumber
        self.repository = repository
    }
    
    func getBalance(onCompleted: @escaping ((String, String)?) -> Void) {
        repository.getBalance(for: cardNumber,
                              onSuccess: { [weak self]  balance in
                                self?.updateBalance(with: balance)
                                onCompleted(self?.getValues()) },
                              onError: { [weak self] in
                                self?.deleteBalance()
                                onCompleted(self?.getValues()) })
    }
    
    private func updateBalance(with currentBalance: Balance)  {
        currentBalance.encodeAndSave(at: Date())
    }
    
    private func deleteBalance() {
        removeEncodedBalance()
    }
    
    func getValues() -> (String, String)? {
        let defaultCurrency = getDefaultCurrency().rawValue
        let referenceCurrency = getReferenceCurrency().rawValue
        guard let balance = Balance.getLastBalance(),
            let defaultValue = balance.currencies[defaultCurrency],
            let referenceValue = balance.currencies[referenceCurrency] else {
            return nil
        }
        
        let formattedDefaultValue = "\(defaultCurrency) \(defaultValue)"
        let formattedReferenceValue = "\(referenceCurrency) \(referenceValue)"
        return  (formattedDefaultValue, formattedReferenceValue)
    }
}
