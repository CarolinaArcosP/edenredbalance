//
//  UIColorExtension.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/4/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

extension UIColor {
    static var darkGray: UIColor {
        return UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1)
    }
    
    static var lightBlack: UIColor {
        return UIColor(red: 64/255, green: 64/255, blue: 64/255, alpha: 1)
    }
    
    static var edenredBlue: UIColor {
        return  UIColor(red: 3/255, green: 155/255, blue: 229/255, alpha: 1)
    }
    
    static var lightEdentedBlue: UIColor  {
        return  UIColor(red: 3/255, green: 155/255, blue: 229/255, alpha: 0.7)
    }
}
