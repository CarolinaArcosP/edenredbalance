//
//  BalanceViewController.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 7/19/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

class EdenredBalanceViewController: BalanceViewController {
    
    // MARK: - Properties
    
    private let _defaultCurrencyPicker = UIPickerView()
    private let _referenceCurrencyPicker = UIPickerView()
    private let _pickerViewData = Currency.allCases.sorted(by: { $0.rawValue < $1.rawValue })
    
    // MARK: - Outlet properties
    
    
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var defaultTitle: UILabel! {
        didSet  {
            defaultTitle.text = "Moneda por defecto"
            defaultTitle.textColor  =  UIColor.darkGray
        }
    }
    @IBOutlet weak var referenceTitle: UILabel!  {
        didSet  {
            referenceTitle.text = "Moneda de referencia"
            referenceTitle.textColor  =  UIColor.darkGray
        }
    }
    @IBOutlet weak var defaultValue: UILabel!  {
        didSet  {
            defaultValue.textColor  =  UIColor.darkGray
        }
    }
    @IBOutlet weak var referenceValue: UILabel!   {
        didSet  {
            referenceValue.textColor  =  UIColor.darkGray
        }
    }
    @IBOutlet weak var defaultCurrency: UITextField! {
        didSet  {
            defaultCurrency.autocorrectionType = .no
            defaultCurrency.textColor = UIColor.darkGray
            defaultCurrency.tintColor = .clear
            defaultCurrency.borderStyle = .none
        }
    }
    @IBOutlet weak var referenceCurrency: UITextField! {
        didSet  {
            referenceCurrency.autocorrectionType = .no
            referenceCurrency.textColor = UIColor.darkGray
            referenceCurrency.tintColor = .clear
            referenceCurrency.borderStyle = .none
        }
    }
    @IBOutlet weak var logoutButton: UIButton!  {
        didSet {
            let attributedTitle = NSAttributedString(string: "CERRAR SESIÓN",
                                                     attributes: [NSAttributedString.Key.foregroundColor : UIColor.edenredBlue,
                                                                  NSAttributedString.Key.underlineStyle : 1])
            logoutButton.setAttributedTitle(attributedTitle, for: .normal)
        }
    }
    @IBOutlet weak var refreshButton: CustomButton! {
        didSet {
            refreshButton.setTitle("ACTUALIZAR", for: .normal)
            refreshButton.setTitleColor(UIColor.white, for: .normal)
        }
    }
    @IBOutlet weak var buttonDistanceConstraint: NSLayoutConstraint!
    @IBOutlet weak var sectionOne: UIStackView!
    @IBOutlet weak var sectionTwo: UIStackView!
    @IBOutlet weak var sectionThree: UIStackView!
    @IBOutlet weak var sectionFour: UIStackView!
    @IBOutlet weak var sectionOneTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var sectionOneTopConstraint: NSLayoutConstraint!

    // MARK: - Initializer
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTouchOutside()
        setupCardImage()
        configureCurrencyPicker()
        
        if validateRequiredUpdate() {
            update()
        } else {
            updateValues(_viewModel.getValues())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotifications(keyboardShow: #selector(keyboardWillShow(notification:)),
                                      keyboardHide: #selector(keyboardWillHide(notification:)))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        unregisterNotifications()
        scrollView.contentInset.bottom = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
        
        sectionOneTrailingConstraint.constant = -(cardImageView.frame.width * 0.25)
        sectionOneTopConstraint.constant = -(cardImageView.frame.height * 0.44)
        refreshButton.applyGradient(colours: [UIColor.lightEdentedBlue, UIColor.edenredBlue])
        
        let excess =  contentView.frame.height - scrollView.frame.height
        if excess < 0 {
            buttonDistanceConstraint.constant = buttonDistanceConstraint.constant - excess
        }
    }
}

// MARK: - Actions
extension EdenredBalanceViewController  {
    @IBAction func showCurrencyPicker(_ sender: Any) {
        defaultCurrency.becomeFirstResponder()
    }
    
    @IBAction func refresh(_ sender: Any) {
        update()
    }
    
    @IBAction func logout(_ sender: Any) {
        resetCardData()
        let loginViewController = LoginViewController(nibName: "LoginView", bundle: nil)
        present(loginViewController, animated: true, completion: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 50
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        scrollView.contentInset.bottom = 0
    }
}

// MARK: - View configuration methods
private extension EdenredBalanceViewController {
    private func configureCurrencyPicker()  {
        let defaultSelectedCurrency = getDefaultCurrency()
        let defaultCurrencyIndex = _pickerViewData.index(of: defaultSelectedCurrency) ?? 0
        _defaultCurrencyPicker.tag = 0
        _defaultCurrencyPicker.delegate = self
        _defaultCurrencyPicker.dataSource = self
        _defaultCurrencyPicker.selectRow(defaultCurrencyIndex, inComponent: 0, animated: false)
        defaultCurrency.text = defaultSelectedCurrency.rawValue
        defaultCurrency.inputView = _defaultCurrencyPicker
        
        let referenceSelectedCurrency = getReferenceCurrency()
        let referenceCurrencyIndex = _pickerViewData.index(of: referenceSelectedCurrency) ?? 0
        _referenceCurrencyPicker.tag = 1
        _referenceCurrencyPicker.delegate = self
        _referenceCurrencyPicker.dataSource = self
        _referenceCurrencyPicker.selectRow(referenceCurrencyIndex, inComponent: 0, animated: false)
        referenceCurrency.text = referenceSelectedCurrency.rawValue
        referenceCurrency.inputView = _referenceCurrencyPicker
    }
    
    private func setupCardImage()  {
        guard cardNumber.count == cardNumberLenght else { return }
        let endSectionOneIndex = cardNumber.index(cardNumber.startIndex, offsetBy: 4)
        let sectionOneString = cardNumber[..<endSectionOneIndex]
        let endSectionTwoIndex = cardNumber.index(endSectionOneIndex, offsetBy: 4)
        let sectionTwoString = cardNumber[endSectionOneIndex..<endSectionTwoIndex]
        let endSectionThreeIndex = cardNumber.index(endSectionTwoIndex, offsetBy: 4)
        let sectionThreeString = cardNumber[endSectionTwoIndex..<endSectionThreeIndex]
        let sectionFourString = cardNumber[endSectionThreeIndex...]
        
        sectionOne.setNumbersInStackview(text: String(sectionOneString))
        sectionTwo.setNumbersInStackview(text: String(sectionTwoString))
        sectionThree.setNumbersInStackview(text: String(sectionThreeString))
        sectionFour.setNumbersInStackview(text: String(sectionFourString))
    }
}

// MARK: - Update methods
private extension EdenredBalanceViewController {
    private func update() {
        updateBalance(
            onLoading: { [weak self] in
                self?.updateRefreshButton(forEnabled: false) },
            onCompleted: { [weak self] values in
                self?.updateValues(values)
                self?.updateRefreshButton(forEnabled: true) }
        )
    }
    
    private func updateValues(_ values: (String, String)?) {
        guard let values = values else {
            defaultValue.text = balanceError
            referenceValue.text = balanceError
            return
        }
        defaultValue.text = values.0
        referenceValue.text = values.1
    }
    
    private func updateRefreshButton(forEnabled: Bool) {
        refreshButton.isEnabled = forEnabled
        refreshButton.alpha = forEnabled  ? 1 : 0.5
    }
}

// MARK: - Picker delegate and data source
extension EdenredBalanceViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return _pickerViewData.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return _pickerViewData[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let currency =  _pickerViewData[row].rawValue
        if pickerView.tag == 0 {
            defaultCurrency.text = currency
            updateDefaultCurrency(currency: currency)
        } else {
            referenceCurrency.text = currency
            updateReferenceCurrency(currency: currency)
        }
        
        updateValues(_viewModel.getValues())
    }
}
