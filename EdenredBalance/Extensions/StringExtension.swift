//
//  StringExtension.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/13/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import Foundation

extension String {
    func isNumeric() -> Bool {
        return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: self))
    }
    
    func toDateWithFormat(_ format: String, locale: Locale = Locale(identifier: "en_US_POSIX")) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
    
    func separate(every: Int, with separator: String) -> String {
        return String(stride(from: 0, to: Array(self).count, by: every).map {
            Array(Array(self)[$0..<min($0 + every, Array(self).count)])
            }.joined(separator: separator))
    }
}
