//
//  BalanceViewController.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/10/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

class BalanceViewController: UIViewController {
    
    // MARK: - Properties
    
    var cardNumber: String = ""
    lazy var _viewModel : BalanceViewModel = {
        return BalanceViewModel(cardNumber: self.cardNumber,
                                repository: BalanceRepository())
    }()
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:  - Private methods
    func validateRequiredUpdate() -> Bool {
        guard let lastUpdate = getLastDate(), Date().timeIntervalSince(lastUpdate) <= 1800 else {
            return true
        }
        
        return false
    }
    
    func updateBalance(onLoading: @escaping() -> Void,
                       onCompleted: @escaping ((String, String)?) -> Void) {
        onLoading()
        _viewModel.getBalance(onCompleted: { values in onCompleted(values) })
    }
}
