//
//  UIImageExtension.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/5/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

extension UIImage  {
    static var wedenred: UIImage {
        return UIImage(imageLiteralResourceName: "wedenred")
    }
    
    static var card: UIImage {
        return UIImage(imageLiteralResourceName: "card")
    }
    
    static var zero: UIImage {
        return UIImage(imageLiteralResourceName: "zero")
    }
    
    static var one: UIImage {
        return UIImage(imageLiteralResourceName: "one")
    }
    
    static var two: UIImage {
        return UIImage(imageLiteralResourceName: "two")
    }
    
    static var three: UIImage {
        return UIImage(imageLiteralResourceName: "three")
    }
    
    static var four: UIImage {
        return UIImage(imageLiteralResourceName: "four")
    }
    
    static var five: UIImage {
        return UIImage(imageLiteralResourceName: "five")
    }
    
    static var six: UIImage {
        return UIImage(imageLiteralResourceName: "six")
    }
    
    static var seven: UIImage {
        return UIImage(imageLiteralResourceName: "seven")
    }
    
    static var eight: UIImage {
        return UIImage(imageLiteralResourceName: "eight")
    }
    
    static var nine: UIImage {
        return UIImage(imageLiteralResourceName: "nine")
    }
}

// MARK: - Image finder

func getImageFor(number: String) -> UIImage? {
    guard let intNumber = Int(number) else { return nil }
    switch intNumber {
    case 0: return UIImage.zero
    case 1: return UIImage.one
    case 2: return UIImage.two
    case 3: return UIImage.three
    case 4: return UIImage.four
    case 5: return UIImage.five
    case 6: return UIImage.six
    case 7: return UIImage.seven
    case 8: return UIImage.eight
    case 9: return UIImage.nine
    default: return nil
    }
}
