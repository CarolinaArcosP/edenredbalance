//
//  BalanceRepository.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 7/19/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import Foundation


class BalanceRepository {
    let balancePath = "https://edenred-budget.herokuapp.com/balance/"
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    func getBalance(for cardNumber: String, onSuccess: @escaping  (Balance) -> Void, onError: @escaping () -> Void) {
        let path = balancePath + cardNumber
        dataTask?.cancel()
        
        if var urlComponents = URLComponents(string: path)  {
            guard let url = urlComponents.url else { return }
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                
                if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    let balance = self.parse(data: data)
                    DispatchQueue.main.async {
                        if let balance = balance { onSuccess(balance) }
                        else { onError()  }
                    }
                } else {
                    DispatchQueue.main.async { onError() }
                }
            }
            
            dataTask?.resume()
        }
    }
    
    private func parse(data: Data) -> Balance? {
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(Balance.self, from:data)
            return result
        } catch {
            return nil
        }
    }
}
