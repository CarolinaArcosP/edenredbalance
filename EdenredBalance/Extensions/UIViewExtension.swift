//
//  UIViewExtension.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/4/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

extension UIButton {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.cornerRadius = buttonCornerRadius
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        layer.insertSublayer(gradient, at: 1)
    }
}
