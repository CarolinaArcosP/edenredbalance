//
//  Balance.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 7/23/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import Foundation

struct Balance: Codable {
    let currencies: [String : String]
    let dollarPrices: [String : Double]
    
    enum CodingKeys: String, CodingKey {
        case currencies = "balance"
        case dollarPrices = "prices"
    }
    
    func getUSDBalance() -> Double? {
        guard let value = currencies[Currency.USD.rawValue], value.count > 1 else { return 0.0 }
        let index = value.index(value.startIndex, offsetBy: 1)
        let mySubstring = String(value[index...])
        return Double(mySubstring)
    }
    
    func encodeAndSave(at date: Date) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(self) {
            saveEncodedBalance(data: encoded, date: date)
        }
    }
    
    static func getLastBalance() -> Balance? {
        guard let data = getCurrentBalanceData() else {
            return nil
        }
        
        return Balance.decode(from: data)
    }
    
    private static func decode(from data: Data) -> Balance? {
        let decoder = JSONDecoder()
        guard let loadedBalance = try? decoder.decode(Balance.self, from: data) else {
            return nil
        }
        return loadedBalance
    }
}
