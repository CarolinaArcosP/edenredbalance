//
//  UIViewControllerExtension.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/8/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTouchOutside() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                                 action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func registerKeyboardNotifications(keyboardShow: Selector, keyboardHide: Selector) {
        NotificationCenter.default.addObserver(self, selector: keyboardShow, name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: keyboardHide, name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
