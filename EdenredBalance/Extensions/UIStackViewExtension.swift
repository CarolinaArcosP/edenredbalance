//
//  UIStackViewExtension.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/13/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

extension UIStackView {
    
    func removeStackViewSubviews()  {
        for subview in arrangedSubviews {
            removeArrangedSubview(subview)
            NSLayoutConstraint.deactivate(subview.constraints)
            subview.removeFromSuperview()
        }
    }
    
    func setNumbersInStackview(text: String) {
        guard text.isNumeric(), text.count <= 4 else { return }
        let numbers = Array(text).map { String($0) }
        let numbersImages = numbers.compactMap { getImageFor(number: $0) }
        setNumbersSection(with: numbersImages)
    }
    
    func setNumbersSection(with numbers: [UIImage]) {
        for numberImage in numbers  {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 19))
            imageView.contentMode = .scaleAspectFit
            imageView.image = numberImage
            addArrangedSubview(imageView)
        }
    }
}
