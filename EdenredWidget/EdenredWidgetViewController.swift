//
//  TodayViewController.swift
//  EdenredWidget
//
//  Created by Carolina Arcos on 8/10/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit
import NotificationCenter

class EdenredWidgetViewController: BalanceViewController, NCWidgetProviding {
    
    // MARK: - Properties
    private var relative17FontConstant: CGFloat = 0.0411
    private var relative15FontConstant: CGFloat = 0.03623188406
    
    @IBOutlet weak var defaultValueLabel: UILabel!
    @IBOutlet weak var referenceValueLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var updateLabel: UILabel!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        defaultValueLabel.font = defaultValueLabel.font.withSize(self.view.frame.width * relative17FontConstant)
        referenceValueLabel.font = referenceValueLabel.font.withSize(self.view.frame.width * relative17FontConstant)
        cardNumberLabel.font = cardNumberLabel.font.withSize(self.view.frame.width * relative15FontConstant)
        updateLabel.font = updateLabel.font.withSize(self.view.frame.width * relative15FontConstant)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupValues()
    }
    
    private func setupValues() {
        guard let cardNumber = getCardNumber(), cardNumber.count == cardNumberLenght else {
            clearValues()
            defaultValueLabel.text = "No hay tarjetas registradas"
            return
        }
        
        self.cardNumber = cardNumber
        updateValues(_viewModel.getValues())
        updateUpdateLabel(forLoading: false)

        if validateRequiredUpdate() {
            update()
        }
    }
    
    private func update() {
        updateBalance(
            onLoading: { [weak self] in
                self?.updateUpdateLabel(forLoading: true) },
            onCompleted: { [weak self] values in
                self?.updateValues(values)
                self?.updateUpdateLabel(forLoading: false)
            }
        )
    }
    
    private func updateValues(_ values: (String, String)?) {
        updateCardText()
        
        guard let values = values else {
            defaultValueLabel.text = balanceError
            referenceValueLabel.text = ""
            return
        }
        defaultValueLabel.text = values.0
        referenceValueLabel.text = values.1
    }
    
    private func updateCardText() {
        let index = cardNumber.index(cardNumber.endIndex, offsetBy: -4)
        let lastdigits = String(cardNumber[index...])
        cardNumberLabel.text = "**** \(lastdigits)"
    }
    
    private func clearValues()  {
        defaultValueLabel.text = ""
        referenceValueLabel.text = ""
        cardNumberLabel.text =  ""
        updateLabel.text = ""
    }
    
    private func updateUpdateLabel(forLoading loading: Bool) {
        if loading {
            updateLabel.text = "Actualizando..."
        } else {
            guard let stringDate = getLastDate()?.toStringWithFormat(visualStringDateFormat) else {
                updateLabel.text = ""
                return
            }
            updateLabel.text = stringDate
        }
    }
}
