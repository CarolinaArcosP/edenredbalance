//
//  UITextFieldExtension.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/11/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

extension UITextField {
    override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(UIResponderStandardEditActions.cut(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
