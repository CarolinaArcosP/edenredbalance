//
//  Global.swift
//  EdenredBalance
//
//  Created by Carolina Arcos on 8/6/19.
//  Copyright © 2019 Carolina Arcos. All rights reserved.
//

import UIKit

// MARK: - Visual constants

let buttonCornerRadius: CGFloat = 22
let cardNumberLenght = 16
let formattedCardNumberLength = 19
let stringDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
let visualStringDateFormat = "HH:mm:ss' - 'dd/MM"

// MARK: - Shared strings

let balanceError = "Error consultando su saldo"

//MARK: - User defaults keys

let userDefaultsSuiteName = "group.com.wolox.WEdenredBalance"
let userDefaultsCardKey = "edenredCardNumber"
let userDefaultsDefaultKey = "edenredDefaultCurrency"
let userDefaultsReferenceKey = "edenredReferenceCurrency"
let userDefaultsBalanceKey = "edenredCurrentBalance"
let userDefaultsUpdateDateKey = "edenredUpdateDate"

// MARK: - User defaults methods

func saveCardNumber(number: String)  {
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.set(number, forKey: userDefaultsCardKey)
}

func saveEncodedBalance(data: Data, date: Date) {
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.set(data, forKey: userDefaultsBalanceKey)
    updateDate(date: date)
}

func updateDate(date: Date) {
    let stringDate = date.toStringWithFormat(stringDateFormat)
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.set(stringDate, forKey: userDefaultsUpdateDateKey)
}

func updateDefaultCurrency(currency: String)  {
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.set(currency,forKey: userDefaultsDefaultKey)
}

func updateReferenceCurrency(currency: String) {
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.set(currency,forKey: userDefaultsReferenceKey)
}

func getCardNumber() -> String? {
    return UserDefaults.init(suiteName: userDefaultsSuiteName)?.value(forKey: userDefaultsCardKey) as? String
}

func getCurrentBalanceData() -> Data?  {
    return UserDefaults.init(suiteName: userDefaultsSuiteName)?.value(forKey: userDefaultsBalanceKey) as? Data
}

func getLastDate() -> Date? {
    guard let stringDate = UserDefaults.init(suiteName: userDefaultsSuiteName)?.value(forKey: userDefaultsUpdateDateKey) as? String else {
        return nil
    }
    return stringDate.toDateWithFormat(stringDateFormat)
}

func getDefaultCurrency() -> Currency {
    guard let currencyString = UserDefaults.init(suiteName: userDefaultsSuiteName)?.string(forKey: userDefaultsDefaultKey),
        let currency = Currency(rawValue: currencyString) else { return Currency.ARS }
    return currency
}

func removeEncodedBalance()  {
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.removeObject(forKey: userDefaultsBalanceKey)
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.removeObject(forKey: userDefaultsUpdateDateKey)
}

func getReferenceCurrency() -> Currency {
    guard let currencyString = UserDefaults.init(suiteName: userDefaultsSuiteName)?.string(forKey: userDefaultsReferenceKey),
        let currency = Currency(rawValue: currencyString) else { return Currency.ARS }
    return currency
}

func resetCardData() {
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.removeObject(forKey: userDefaultsCardKey)
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.removeObject(forKey: userDefaultsDefaultKey)
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.removeObject(forKey: userDefaultsReferenceKey)
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.removeObject(forKey: userDefaultsBalanceKey)
    UserDefaults.init(suiteName: userDefaultsSuiteName)?.removeObject(forKey: userDefaultsUpdateDateKey)
}

//  MARK: - Currencies

enum Currency: String, CaseIterable {
    case USD
    case ARS
    case CLP
    case MXN
    case COP
    case BRL
    case PEN
}
